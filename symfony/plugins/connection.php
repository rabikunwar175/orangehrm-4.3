<?php
$conn = mysqli_connect("localhost","root","","orange_mysql");
if ($conn->connect_error) {
	die("connection failed" . $conn->connect_error);
	
}
// echo "connected";

?>



<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php if ($attendancePermissions->canRead()) { ?>
    <div class="box" id="attendance-summary">
        <div class="head"><h1><?php echo __('Attendance Total Summary Report'); ?></h1></div>
        <div class="inner">
            <?php include_partial('global/flash_messages'); ?>
            <form action="" method="post">

                <fieldset>
                    <ol>

                       <!--  <li>
                         <input id="employee_name" type="text" name="attendanceTotalSummary[empName]" class="ac_input" value="All" style="display: none;">
                        </li> -->
                       <li> 
                        <label><?php echo __('Employee Name'.' <em>*</em>') ?></label>
                        <?php echo $form['empName']->render(); ?>
                    
                    </li>

                        <li>
                            <label><?php echo __('Job Title') ?></label>
                            <?php echo $form['jobTitle']->renderError() ?><?php echo $form['jobTitle']->render(); ?>
                        </li>

                        <li>
                            <label><?php echo __('Sub Unit') ?></label>
                            <?php echo $form['subUnit']->renderError() ?><?php echo $form['subUnit']->render(); ?>
                        </li>

                        <li>
                            <label><?php echo __('Employment Status') ?></label>
                            <?php echo $form['employeeStatus']->renderError() ?><?php echo $form['employeeStatus']->render(); ?>
                        </li>
                        <li>
                            <label><?php echo __('From') ?></label>
                            <?php echo $form['fromDate']->render(); ?>
                        </li>
                        <li>
                            <label><?php echo __('To') ?></label>
                            <?php echo $form['toDate']->render(); ?>
                        </li>

                        <li class="required">
                            <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                        </li>

                    </ol>

                    <p>
                        <input type="submit" name="viewbutton" id="viewbutton" value="<?php echo __('View') ?>"/>

                    </p>


                    <?php echo $form->renderHiddenFields(); ?>

                </fieldset>
            </form>
        </div>
    </div>
<?php } ?>


<?php
include_once(dirname(__FILE__).'\..\..\..\..\connection.php');

if(isset($_POST['viewbutton'])){
$requestAll = $_POST;


$fromDate = date('Y-m-d H:i:s', strtotime($requestAll['attendanceTotalSummary']['fromDate']));
$toDate = date('Y-m-d H:i:s', strtotime($requestAll['attendanceTotalSummary']['toDate']));

$dateStrVal = strtotime($fromDate);
if(empty(strtotime($fromDate)) && empty(strtotime($toDate)))
{
    $sql = "SELECT ohrm_user.user_name,ohrm_attendance_record.punch_in_user_time,ohrm_attendance_record.punch_in_note,ohrm_attendance_record.punch_out_user_time,ohrm_attendance_record.punch_out_note,TIMESTAMPDIFF(MINUTE, punch_in_utc_time, punch_out_utc_time) as duration FROM ohrm_attendance_record join ohrm_user on ohrm_attendance_record.employee_id = ohrm_user.emp_number";
    
    // $sql = "SELECT (hs_hr_employee.emp_firstname,hs_hr_employee.emp_middlename,hs_hr_employee.emp_lastname) as employee ohrm_attendance_record.punch_in_user_time,ohrm_attendance_record.punch_in_note,ohrm_attendance_record.punch_out_user_time,ohrm_attendance_record.punch_out_note,TIMESTAMPDIFF(MINUTE, punch_in_utc_time, punch_out_utc_time) as duration FROM ohrm_attendance_record join ohrm_user on ohrm_attendance_record.employee_id = hs_hr_employee.employee_id";
    
}
else{
    $sql = "SELECT ohrm_user.user_name,ohrm_attendance_record.punch_in_user_time,ohrm_attendance_record.punch_in_note,ohrm_attendance_record.punch_out_user_time,ohrm_attendance_record.punch_out_note,TIMESTAMPDIFF(MINUTE, punch_in_utc_time, punch_out_utc_time) as duration FROM ohrm_attendance_record join ohrm_user on ohrm_attendance_record.employee_id = ohrm_user.emp_number WHERE `punch_in_utc_time` BETWEEN '$fromDate' AND '$toDate'";
     // $sql = "SELECT hs_hr_employee.emp_firstname,hs_hr_employee.emp_middlename,hs_hr_employee.emp_lastname  ohrm_attendance_record.punch_in_user_time,ohrm_attendance_record.punch_in_note,ohrm_attendance_record.punch_out_user_time,ohrm_attendance_record.punch_out_note,TIMESTAMPDIFF(MINUTE, punch_in_utc_time, punch_out_utc_time) as duration FROM ohrm_attendance_record join hs_hr_employee on ohrm_attendance_record.employee_id = hs_hr_employee.emp_number WHERE `punch_in_utc_time` BETWEEN '$fromDate' AND '$toDate' ";
}
$result = mysqli_query($conn, $sql);
}
?>
<?php if (isset($_POST['viewbutton'])) { ?>
    
    <div id="recordsTable">
    <div id="msg" ></div>
    <div class="box noHeader" id="search-results">
<div id="helpText" class="helpText"></div>
<div class="inner">

<div id="scrollWrapper">
    <div id="scrollContainer">
    </div>
</div>
<div id="tableWrapper">
    <table class="table hover" id="resultTable">
        <thead>
            <tr>
                <th rowspan="1" style="width:20%"><span class="headerCell">Employee Name</span></th>
                <th rowspan="1" style="width:20%"><span class="headerCell">Punch In</span></th>
                <th rowspan="1" style="width:15%"><span class="headerCell">Punch In Note</span></th>
                <th rowspan="1" style="width:20%"><span class="headerCell">Punch Out</span></th>
                <th rowspan="1" style="width:15%"><span class="headerCell">Punch Out Note</span></th>
                 <th rowspan="1" style="width:15%"><span class="headerCell">Duration(Hours)</span></th>
            </tr>            
        </thead>
        <tbody>
                    <?php
                    if (mysqli_num_rows($result) > 0) {
                        foreach ($result as $key => $value) {
                            ?>
                    <tr>
                         <td><?= $value['user_name']; ?></td>

  
<!--                         <td><?= $value['emp_lastname']; ?></td>
 -->                     <td><?= $value['punch_in_user_time']; ?></td>
                        <td><?= $value['punch_in_note']?$value['punch_in_note']:'N/A'; ?></td>
                        <td><?= $value['punch_out_user_time']; ?></td>
                        <td><?= $value['punch_out_note']?$value['punch_out_note']:'N/A';?></td>
                        <td><?= round((int)$value['duration']/60, 2); ?></td>
                    </tr>
                    <?php

                }
            } else {
                ?>
                    <tr>
                        <td colspan="6">No Records Found</td>
                    </tr>
                    <?php

                }
                ?>

                </tbody>
    </table>
</div>

</div>

</div>
<?php } ?>
<script type="text/javascript">
    var datepickerDateFormat = '<?php echo get_datepicker_date_format($sf_user->getDateFormat()); ?>';
    var lang_dateError = '<?php echo __("To date should be after from date") ?>';
    var lang_invalidDate = '<?php echo __(ValidationMessages::DATE_FORMAT_INVALID, array('%format%' => str_replace('yy', 'yyyy', get_datepicker_date_format($sf_user->getDateFormat())))) ?>';
    var lang_emptyEmployee = '<?php echo __('Select an Employee')?>';
    var lang_required = '<?php echo __(ValidationMessages::REQUIRED);?>';
    var lang_invalid = '<?php echo __(ValidationMessages::INVALID);?>';
    var employees = <?php echo str_replace('&#039;', "'", $form->getEmployeeListAsJson()) ?> ;

    var employeesArray = eval(employees);
    var errorMsge;
    var employeeFlag;

    $(document).ready(function () {

        if (<?php echo $lastEmpNumber; ?> == '-1'
    )
        {
            $("#employee_name").val('<?php echo __('All')?>');
            $('#attendanceTotalSummary_employeeId').val('-1');
        }
    else
        {
            if ($("#employee_name").val() == '') {
                $("#employee_name").val('<?php echo __("Type for hints") . "..."; ?>').addClass("inputFormatHint");
            }

            $('#viewbutton').click(function () {
                $('#attendanceTotalSummaryReportForm input.inputFormatHint').val('');
                $('#attendanceTotalSummaryReportForm').submit();
            });

            $("#employee_name").one('focus', function () {

                if ($(this).hasClass("inputFormatHint")) {
                    $(this).val("");
                    $(this).removeClass("inputFormatHint");
                }
            });
        }

        $("#employee_name").autocomplete(employees, {

            formatItem: function (item) {
                return $('<div/>').text(item.name).html();
            },
            formatResult: function (item) {
                return item.name
            }
            , matchContains: true
        }).result(function (event, item) {
                $(this).valid();
            }
        );

        $('#attendanceTotalSummaryReportForm').submit(function () {
            $('#validationMsg').removeAttr('class');
            $('#validationMsg').html("");
            var employeeFlag = validateInput();
            if (!employeeFlag) {
                if (errorMsge) {
                    $('#validationMsg').attr('class', "messageBalloon_failure");
                    $('#validationMsg').html(errorMsge);
                }
                return false;
            }
        });

        //Validation
        $("#attendanceTotalSummaryReportForm").validate({
            rules: {
                'attendanceTotalSummary[empName]': {
                    required: true,
                    employeeValidation: true,
                    onkeyup: false
                },
                'attendanceTotalSummary[fromDate]': {
                    valid_date: function () {
                        return {
                            format: datepickerDateFormat,
                            required: true,
                            displayFormat: displayDateFormat
                        }
                    }
                },
                'attendanceTotalSummary[toDate]': {
                    valid_date: function () {
                        return {
                            format: datepickerDateFormat,
                            required: true,
                            displayFormat: displayDateFormat
                        }
                    },
                    date_range: function () {
                        return {
                            format: datepickerDateFormat,
                            displayFormat: displayDateFormat,
                            fromDate: $('#from_date').val()
                        }
                    }
                }
            },
            messages: {
                'attendanceTotalSummary[empName]': {
                    required: lang_required,
                    employeeValidation: lang_invalid
                },
                'attendanceTotalSummary[fromDate]': {
                    valid_date: lang_invalidDate
                },
                'attendanceTotalSummary[toDate]': {
                    valid_date: lang_invalidDate,
                    date_range: lang_dateError
                }
            }
        });


        if ($("#employee_name").val() == '') {
            $("#employee_name").val('<?php echo __("Type for hints") . "..."; ?>')
                .addClass("inputFormatHint");
        }

        $('#viewbutton').click(function () {
            $('#attendanceTotalSummaryReportForm input.inputFormatHint').val('');
            $('#attendanceTotalSummaryReportForm').submit();
        });

        $("#employee_name").one('focus', function () {

            if ($(this).hasClass("inputFormatHint")) {
                $(this).val("");
                $(this).removeClass("inputFormatHint");
            }
        });
    });
    
    function reformatDate($date, $fromDate = 'Y-m-d', $toDate = 'd-m-Y') {
        $date_aux = date_create_from_format($fromDate, $date);
        return date_format($date_aux,$toDate);
    }
    function validateInput() {

        var errorStyle = "background-color:#FFDFDF;";
        var empDateCount = employeesArray.length;
        var temp = false;
        var i;
        errorMsge = null;
        if (empDateCount == 0) {

            errorMsge = '<?php echo __("No Employees Available");?>';
            return false;
        }
        for (i = 0; i < empDateCount; i++) {
            empName = $.trim($('#employee_name').val()).toLowerCase();
            arrayName = employeesArray[i].name.toLowerCase();

            if (empName == arrayName) {
                $('#attendanceTotalSummary_employeeId').val(employeesArray[i].id);
                temp = true;
                break;
            }
        }
        if (temp) {
            return true;
        }
    }

    $.validator.addMethod("employeeValidation", function (value, element, params) {

        var empCount = employeesArray.length;
        var isValid = false;
        var empName = $('#employee_name').val();
        var inputName = $.trim(empName).toLowerCase();
        if (inputName != "") {
            var i;
            for (i = 0; i < empCount; i++) {
                var arrayName = employeesArray[i].name.toLowerCase();
                if (inputName == arrayName) {
                    isValid = true;
                    break;
                }
            }
        }
        return isValid;
    });
</script>

