<?php

// $recCount = count($leaveList);

$userId = $sf_user->getAttribute('auth.userId');
$loggedInUser = $sf_user->getAttribute('auth.userRoleId');
$leaveData = getAssignedLeaveReqData($userId,$loggedInUser);
$recCount = count($leaveData);
// print_r($loggedInUser);

function getAssignedLeaveReqData($userId,$loggedInUser){
    $conn = mysqli_connect("localhost", "root", "", "orange_mysql");

    // include_once(dirname(__FILE__).'\..\..\..\..\connection.php');
    $getLeaveData = [];
    

    if ($loggedInUser == 1) {
    
        $sql2 = "SELECT * 
        FROM `hs_hr_emp_leave_status` 
        where `hs_hr_emp_leave_status`.`status` = '1' 
        ORDER BY `hs_hr_emp_leave_status`.`id` DESC";

        $getLeaveData = mysqli_query($conn, $sql2);
        // foreach ($getLeaveData1 as $key => $value) { 
        //     echo "<pre>";
        //     print_r($value);
        //     die('tete');
        // }


        
    }
    if ($loggedInUser == 2) {
        $sql1 = "SELECT * 
        FROM `hs_hr_emp_leave_status` 
        where `hs_hr_emp_leave_status`.`sup_emp_number` =  '$userId' 
        and `hs_hr_emp_leave_status`.`status` = '0' 
        ORDER BY `hs_hr_emp_leave_status`.`id` DESC";

        $getLeaveData = mysqli_query($conn, $sql1);

    }

    
    return $getLeaveData;
    
    // die("test");
}

function getLeaveDataListData($userId,$loggedInUser)
{
    // die('me');
    $conn = mysqli_connect("localhost", "root", "", "orange_mysql");

    // require(dirname(__FILE__).'\..\..\..\..\connection.php');
    $getLeaveData = [];
    

    if ($loggedInUser == 1) { //admin is 1
        $sql1 = "SELECT DISTINCT 
                    `hs_hr_emp_leave_status`.`leave_request_id`,
                    `ohrm_leave_request`.`date_applied`, 
                    `hs_hr_employee`.`emp_firstname`, 
                    `hs_hr_employee`.`emp_lastname`,
                    `ohrm_leave`.`status` 
                FROM 
                    `hs_hr_emp_leave_status` 
                JOIN `ohrm_leave_request` ON `hs_hr_emp_leave_status`.`leave_request_id` = `ohrm_leave_request`.`id` 
                JOIN `ohrm_leave` ON `ohrm_leave`.`leave_request_id` = `hs_hr_emp_leave_status`.`leave_request_id` 
                JOIN `hs_hr_employee` ON `hs_hr_emp_leave_status`.`emp_number` = `hs_hr_employee`.`emp_number` 
                JOIN `hs_hr_emp_leave_status` e1 ON e1.leave_request_id = hs_hr_emp_leave_status.leave_request_id 
                AND e1.sup_emp_number <> hs_hr_emp_leave_status.sup_emp_number
                WHERE 
                    `hs_hr_emp_leave_status`.`status` = 1
                    AND e1.status =  1 
                    AND ohrm_leave.status !=  -1
                    AND ohrm_leave.status !=  1
                    AND ohrm_leave.status !=  2 ";
        
        $getLeaveData = mysqli_query($conn, $sql1);
        foreach ($getLeaveData1 as $key => $value) {
        echo "<pre>";
        print_r($value);
        // die('here');
        }
        
    }

    if ($loggedInUser == 2) { //supervisor is 2 as well as employee
        $sql = "SELECT
         `hs_hr_emp_leave_status`.`leave_request_id`,
         `ohrm_leave_request`.`date_applied`,
         `hs_hr_employee`.`emp_firstname`,
         `hs_hr_employee`.`emp_lastname`,
         `hs_hr_emp_leave_status`.`sup_emp_number` 
         FROM `hs_hr_emp_leave_status` 
         JOIN `ohrm_leave_request` ON `hs_hr_emp_leave_status`.`leave_request_id` = `ohrm_leave_request`.`id` 
         JOIN `ohrm_leave` ON `ohrm_leave`.`leave_request_id` = `hs_hr_emp_leave_status`.`leave_request_id` 
         JOIN `hs_hr_employee` ON `hs_hr_emp_leave_status`.`emp_number` = `hs_hr_employee`.`emp_number`
         AND `hs_hr_emp_leave_status`.`status` = '0' 
         AND `hs_hr_emp_leave_status`.`sup_emp_number` = '$userId' ";
        $getLeaveData = mysqli_query($conn, $sql);
        
    }

    
    return $getLeaveData;
   
}

?>
<div id="task-list-group-panel-container" class="" style="height:100%; ">
    <div id="task-list-group-panel-menu_holder" class="task-list-group-panel-menu_holder" style="height:85%; overflow-x: hidden; overflow-y: auto;">
        <table class="table hover">
            <tbody>
                <?php
                if ($recCount > 0):
                    $count = 0;

                    foreach (getLeaveDataListData($userId,$loggedInUser) as $key => $leaveData):
                        ?>
                        <tr class="<?php echo ($count & 1) ? 'even' : 'odd' ?>">
                            <td>
                            
                                <a href="<?php echo url_for('leave/viewLeaveRequest') . '?id=' . $leaveData['leave_request_id'] ?>">
                                    <?php
                                    $count++;
                                    echo str_pad($count, 2, '0', STR_PAD_LEFT) . ". " . $leaveData['emp_firstname'] . " " . $leaveData['emp_lastname'] . " . " . set_datepicker_date_format($leaveData['date_applied']);
                                    ?>
                                </a>
                               
                            </td>
                        </tr>
                    <?php endforeach; ?>
                                        
                <?php else: ?>
                        <tr class="odd"><td><?php echo __(DashboardService::NO_REC_MESSAGE); ?></td></tr>
                <?php endif; ?>
            </tbody>  
        </table>
    </div>
    <div id="total" >
        <table class="table">
            <tr class="total">
                <td style="text-align:left;padding-left:20px; cursor: pointer"> 
                    <?php
                    $date = pendingLeaveRequestsAction::getDateRange();
                    $dateRange = set_datepicker_date_format($date->getFromDate()) .' '. __('to') . ' ' .set_datepicker_date_format($date->getToDate());
                    echo ' <span title = "' . $dateRange . '">' . __('%months% month(s)', array('%months%' => 3)) . '</span>';
                    ?>
                </td>
                <td style="text-align:right;padding-right:20px;"> 
                    <?php
                    echo __('Total : ') . $recCount . ' / ' . $recordCount;
                    ?>
                </td>                
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // hover color change effect
        $("#task-list-group-panel-slider li").hover(function() {
            $(this).animate({opacity: 0.90}, 100, function(){ 
                $(this).animate({opacity: 1}, 0);
            } );
        });     
    });

</script>