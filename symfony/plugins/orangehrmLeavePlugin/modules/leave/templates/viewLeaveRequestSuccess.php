<?php use_javascript(plugin_web_path('orangehrmLeavePlugin', 'js/viewLeaveRequestSuccess.js'));
// die('me');
?>

<?php 
$userId = $sf_user->getAttribute('auth.userId');
$loggedInUser = $sf_user->getAttribute('auth.userRoleId');


if($leaveListPermissions->canRead()){?>
<div id="processing"></div>

<!--this is ajax message place -->
<div id="msgPlace"></div>
<!-- end of ajax message place -->
<?php
$conn = mysqli_connect("localhost", "root", "", "orange_mysql");

// require(dirname(__FILE__).'\..\..\..\..\connection.php');
$leave_request_id = $_GET['id'];
if (isset($_POST['btnSave'])) {

    if(isset($_POST['select_leave_action_50'])){
        switch ($_POST['select_leave_action_50']) {

            case '':
                die('select option');
                break;

            case '90': //approve

                if ($loggedInUser == 1) {
                    $updateStatusApproveAdminSql = "UPDATE ohrm_leave 
                    SET status = '1' 
                    WHERE leave_request_id = $leave_request_id";
                    $updateStatusApproveAdmin = mysqli_query($conn,$updateStatusApproveAdminSql);

               }

               if ($loggedInUser == 2) {           
                    $updateStatusApproveSql = "UPDATE hs_hr_emp_leave_status 
                    SET status = '1' 
                    WHERE leave_request_id = $leave_request_id 
                    AND sup_emp_number = $userId";
                    $updateStatusApprove = mysqli_query($conn,$updateStatusApproveSql);
                // print_r($updateStatusApprove);
                // die('test');
               }

                break;

            case '93': //cancelled
        
            if ($loggedInUser == 1) {
                $updateStatusApproveAdminSql = "UPDATE ohrm_leave 
                SET status = '2' 
                WHERE leave_request_id = $leave_request_id";
                $updateStatusApproveAdmin = mysqli_query($conn,$updateStatusApproveAdminSql);
            }

            if ($loggedInUser == 2) {
                $updateStatusApproveSql = "UPDATE hs_hr_emp_leave_status 
                SET status = '2' 
                WHERE leave_request_id = $leave_request_id 
                AND sup_emp_number = $userId";
                $updateStatusApprove = mysqli_query($conn,$updateStatusApproveSql);
            }
                
                break;

            case '95': // rejected
            if ($loggedInUser == 1) {
                $updateStatusApproveAdminSql = "UPDATE ohrm_leave 
                SET status = '-1' 
                WHERE leave_request_id = $leave_request_id";
                $updateStatusApproveAdmin = mysqli_query($conn,$updateStatusApproveAdminSql);
            }

            if ($loggedInUser == 2) {
                $updateStatusApproveSql = "UPDATE hs_hr_emp_leave_status 
                SET status = '-1' 
                WHERE leave_request_id = $leave_request_id 
                AND sup_emp_number = $userId";
                $updateStatusApprove = mysqli_query($conn,$updateStatusApproveSql);
            }
                break;        
            
            default:
                # code...
                break;
        }
        // print_r($_POST['select_leave_action_50']);
        //  die("herer");
    }
}
 ?>

<?php

$userId = $sf_user->getAttribute('auth.userId');
$loggedInUser = $sf_user->getAttribute('auth.userRoleId');
$leave_request_id = $_GET['id'];
// print_r($leave_request_id);
// 

// $leaveData = getAssignedLeaveReqData($userId,$loggedInUser);
// die('me');
// // $recCount = count($leaveData);
$conn = mysqli_connect("localhost", "root", "", "orange_mysql");

// require(dirname(__FILE__).'\..\..\..\..\connection.php');
    $getLeaveData = [];

    if ($loggedInUser == 1) {

        $sql = "SELECT 
        hs_hr_employee.emp_firstname,
        hs_hr_employee.emp_lastname,
        ohrm_leave.leave_request_id,
        ohrm_leave.length_hours,
        ohrm_leave.date,
        ohrm_leave_type.name AS leave_type_name,
        ohrm_leave_status.name,
        ohrm_leave_entitlement.no_of_days,
        ohrm_leave_entitlement.days_used,
        ohrm_leave_entitlement.no_of_days-ohrm_leave_entitlement.days_used AS Difference
        FROM ohrm_leave 
        JOIN hs_hr_employee ON hs_hr_employee.emp_number = ohrm_leave.emp_number 
        JOIN ohrm_leave_type ON ohrm_leave.leave_type_id = ohrm_leave_type.id 
        JOIN ohrm_leave_entitlement ON ohrm_leave_entitlement.emp_number = ohrm_leave.emp_number 
        JOIN ohrm_leave_status ON ohrm_leave_status.status = ohrm_leave.status 
        WHERE ohrm_leave.leave_request_id = $leave_request_id ";

        $getLeaveData = mysqli_query($conn, $sql);

        foreach ($getLeaveData as $key => $value) {
            // echo "<pre>";
            // print_r($value);
        }
        // echo "<pre>";
        // print_r($getLeaveData);
        // die('me');

    }


    if ($loggedInUser == 2) {

        $sql = "SELECT 
            hs_hr_emp_leave_status.emp_number,
            hs_hr_employee.emp_firstname,
            hs_hr_employee.emp_lastname,
            ohrm_leave.leave_request_id,
            ohrm_leave.length_hours,
            ohrm_leave.date,
            ohrm_leave_type.name AS leave_type_name,
            ohrm_leave_status.name,
            ohrm_leave_entitlement.no_of_days,
            ohrm_leave_entitlement.days_used,
            ohrm_leave_entitlement.no_of_days-ohrm_leave_entitlement.days_used AS Difference 
            FROM hs_hr_emp_leave_status 
            JOIN ohrm_leave ON ohrm_leave.leave_request_id = hs_hr_emp_leave_status.leave_request_id 
            JOIN hs_hr_employee ON hs_hr_employee.emp_number = hs_hr_emp_leave_status.emp_number 
            JOIN ohrm_leave_type ON ohrm_leave.leave_type_id = ohrm_leave_type.id 
            JOIN ohrm_leave_entitlement ON ohrm_leave_entitlement.emp_number = hs_hr_emp_leave_status.emp_number 
            JOIN ohrm_leave_status ON ohrm_leave_status.status = hs_hr_emp_leave_status.status 
            WHERE hs_hr_emp_leave_status.leave_request_id = $leave_request_id
            AND hs_hr_emp_leave_status.sup_emp_number = $userId";

        $getLeaveData = mysqli_query($conn, $sql);

        foreach ($getLeaveData as $key => $value) {
            // echo "<pre>";
            // print_r($value);
        }
        // echo "<pre>";
        // print_r($getLeaveData);
        // die('me');

    }
    

 ?>
<!-- start of table  -->
<div class="box" id="search-results">
    <div class="head"><h1><?php echo str_pad($count, 1) ."Leave Request". " (" . $value['date'] . ") " . $value['emp_firstname'] . " " . $value['emp_lastname'] ?></h1></div>

<div class="inner">
    <div style="padding-left: 5px; padding-top: 5px;">
        <a href="#" id="view_request_comments">View Leave Request Comments</a>
    </div>
    <form method="post" action="">
        <input type="hidden" name="defaultList[_csrf_token]" value="" id="defaultList__csrf_token" />

        <div id="helpText" class="helpText"></div>


        <div id="scrollWrapper">
            <div id="scrollContainer"></div>
        </div>        
        <div id="tableWrapper">
            <table class="table hover" id="resultTable">
                <thead>
                    <tr>
                        <th rowspan="1" style="width:20%"><span class="headerCell">Date</span></th>
                        <th rowspan="1" style="width:10%"><span class="headerCell">Leave Type</span></th>
                        <th rowspan="1" style="width:10%"><span class="headerCell">Leave Balance (Days)</span></th>
                        <th rowspan="1" style="width:8%"><span class="headerCell">Duration (Hours)</span></th>
                        <th rowspan="1" style="width:12%"><span class="headerCell">Status</span></th>
                        <th rowspan="1" style="width:30%"><span class="headerCell">Comments</span></th>
                        <th rowspan="1" style="width:10%"><span class="headerCell">Actions</span></th>
                        </tr>            
                </thead>
                    <tbody>

                        <?php




                        ?>
                        <tr class="odd">
                                <td class="left" ><?php echo $value['date']; ?></td>
                                <td class="left" ><?php echo $value['leave_type_name']; ?></td>
                                <td class="right" ><?php echo $value['Difference']; ?></td>
                                <td class="right" ><?php echo $value['length_hours']; ?></td>
                                <td class="left" ><?php echo $value['name']; ?><input type="hidden" name="leave[50]" id="leave-50" class="requestIdHolder" value="Non Working Day" /></td>
                                <td class="left" ><span class="commentContainerLong"><span id="commentContainer-50"></span><img src="/orangehrm-4.0/symfony/web/webres_598bd8c4489f52.47381308/themes/default/images/callout-left.png" title="Click here to edit" alt="Edit" class="callout dialogInvoker" /><input type="hidden" id="hdnLeaveComment-50" name="leaveComments[50]" value="" /></span><input type="hidden" name="leave[50]" id="hdnLeave_50" class="" value="50" /></td>
                                <td class="left" ><select id="select_leave_action_50" name="select_leave_action_50" class="select_action quotaSelect">
                                    <option value="">Select Action</option>
                                    <option value="90">Approve</option>
                                    <option value="93">Cancel</option>
                                    <option value="95">Reject</option>
                                    </select><input type="hidden" name="10-1" id="10-1" class="quotaHolder" value="1" /></td>
                        </tr>
                    </tbody>
            </table>
        </div> <!-- tableWrapper -->
        <div class="bottom">
            <input type="submit" class="" id="btnSave" name="btnSave" value="Save" />
            <input type="submit" class="reset" id="btnBack" name="btnBack" value="Back" />
        </div>
    </form> <!-- frmList_ohrmListComponent --> 
</div> <!-- inner -->
</div> <!-- search close-->
        
<!--End of table -->



<!-- <?php echo "me"; ?> -->
<!-- comment dialog -->
<div class="modal midsize hide" id="commentDialog">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3><?php echo __('Leave Comments'); ?></h3>
  </div>
  <div class="modal-body">
    <p>
    <form action="updateComment" method="post" id="frmCommentSave">
        <input type="hidden" id="leaveId" />
        <input type="hidden" id="leaveOrRequest" />        
        <?php echo $leavecommentForm ?>
        <div id="existingComments">  
            <span><?php echo __('Loading') . '...';?></span>
        </div>
        <?php if ($commentPermissions->canCreate()):?>
        <br class="clear" />
        <br class="clear" />
        <textarea name="leaveComment" id="leaveComment" cols="40" rows="4" class="commentTextArea"></textarea>
        <span id="commentError"></span>
        <?php endif;?>
    </form>        
    </p>
  </div>
  <div class="modal-footer">
    <?php if ($commentPermissions->canCreate()):?>
    <input type="button" class="btn" id="commentSave" value="<?php echo __('Save'); ?>" />
    <?php endif;?>
    <input type="button" class="btn reset" data-dismiss="modal" id="commentCancel" value="<?php echo __('Cancel'); ?>" />
  </div>
</div>
<!-- end of comment dialog-->
<?php }?>



<script type="text/javascript">
    //<![CDATA[

    var leaveRequestId = <?php echo $leaveRequestId; ?>;
    var leave_status_pending = 'Pending Approval'; // TO DO: Fix, check if compatible with localization
    var ess_mode = '<?php echo ($essMode) ? '1' : '0'; ?>';
    var lang_Required = '<?php echo __(ValidationMessages::REQUIRED);?>';
    var lang_comment_successfully_saved = '<?php echo __(TopLevelMessages::SAVE_SUCCESS); ?>';
    var lang_comment_save_failed = '<?php echo __(TopLevelMessages::SAVE_FAILURE); ?>'; 
    var lang_Processing = '<?php echo __('Processing'); ?>...';
    var lang_Close = '<?php echo __('Close');?>';
    var lang_Date = '<?php echo __('Date');?>';
    var lang_Time = '<?php echo __('Time');?>';
    var lang_Author = '<?php echo __('Author');?>';
    var lang_Comment = '<?php echo __('Comment');?>';
    var lang_Loading = '<?php echo __('Loading');?>...';
    var lang_LengthExceeded = '<?php echo __(ValidationMessages::TEXT_LENGTH_EXCEEDS, array('%amount%' => 255)); ?>';
    var lang_LeaveComments = '<?php echo __('Leave Comments'); ?>';
    var lang_LeaveRequestComments = '<?php echo __('Leave Request Comments'); ?>';
    var lang_selectAction = '<?php echo __("Select Action");?>';
    var lang_Close = '<?php echo __('Close');?>';    
    var getCommentsUrl = '<?php echo url_for('leave/getLeaveCommentsAjax'); ?>';
    var commentUpdateUrl = '<?php echo public_path('index.php/leave/updateComment'); ?>';
    var backUrl = '<?php echo url_for($backUrl); ?>';     
    //]]>
</script>

