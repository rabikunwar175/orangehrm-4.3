<?php
 /**
 * RoomService class file
 */

/**
 * Room Service
 * @package room
 * @todo All methods to return PIMServiceException or DaoException consistantly [DONE]
 * @todo Don't wrap DAO exceptions. [DONE]
 * @todo Deside if all methods need to have try catch blocks [DONE]
 * @todo Show class hierarchy (inheritance) of all the classes in the API
 */
class RoomService extends BaseService {

    /**
     * @ignore
     */
    private $roomDao;

    /**
     * Get Employee Dao
     * @return EmployeeDao
     * @ignore
     */
    public function getRoomDao() {
        return $this->roomDao;
    }

    /**
     * Set Employee Dao
     * @param EmployeeDao $employeeDao
     * @return void
     * @ignore
     */
    public function setRoomDao(RoomDao $roomDao) {
        die('testing inside service');
        $this->roomDao = $roomDao;
    }
    
    /**
     * Construct
     * @ignore
     */
    public function __construct() {
        die('testing inside service');
        $this->roomDao = new RoomDao();
    }
}
