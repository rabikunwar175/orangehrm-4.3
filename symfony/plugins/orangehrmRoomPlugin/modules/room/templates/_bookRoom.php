<?php
$conn = mysqli_connect("localhost", "root", "", "orange_mysql");
if ($_POST['btnSave']) {
    try {
        $bookedBy      = $_SESSION['user'];
        $meetingRoomId = $_POST['meeting_room_id'];
        $date          = $_POST['date'];
        $fromTime      = $_POST['from_time'];
        $toTime        = $_POST['to_time'];
        $fromDateTime  = $date . ' ' . $fromTime;
        $toDateTime    = $date . ' ' . $toTime;
        $isValidated   = validateBookingTime($conn, $meetingRoomId, $fromDateTime, $toDateTime);
        if (!$isValidated) {
            $_SESSION["error_msg"] = "The meeting room is already booked for this period.";
            return false;
        }
        $bookedBy = $_SESSION['user'];
        $sql      = "INSERT INTO ohrm_bookings (meeting_room_id, booked_by, from_time, to_time) VALUES('$meetingRoomId', '$bookedBy','$fromDateTime','$toDateTime')";
        $result   = mysqli_query($conn, $sql);
        if ($result) {
            $_SESSION["success_msg"] = "Successfully booked the meeting room.";
        } else {
            $_SESSION["error_msg"] = "Failed to book the meeting room.";
        }
        return $result;
    } catch (\Exception $exception) {
        $_SESSION["error_msg"] = $exception->getMessage();
        return false;
    }
}

function getMeetingRoomsList($conn)
{
    $sql    = "SELECT * FROM `ohrm_meeting`";
    $result = mysqli_query($conn, $sql);
    return $result;
}

function validateBookingTime($conn, $meetingRoomId, $fromDateTime, $toDateTime)
{
    $sql    = "SELECT meeting_room_id FROM `vw_meeting_rooms_with_bookings` WHERE `meeting_room_id` ='$meetingRoomId' AND (`from_time` BETWEEN '$fromDateTime' AND '$toDateTime' OR `to_time` BETWEEN '$fromDateTime' AND '$toDateTime') OR (`from_time` = '$fromDateTime') OR (`to_time` = '$toDateTime')";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        return false;
    }
    return true;
}
