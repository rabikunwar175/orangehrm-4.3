<?php include('_addRoom.php'); ?>
<?php echo stylesheet_tag(plugin_web_path('orangehrmDashboardPlugin', 'css/orangehrmDashboardPlugin.css')); ?>
<style type="text/css">
    .loadmask {
        top: 0;
        left: 0;
        -moz-opacity: 0.5;
        opacity: .50;
        filter: alpha(opacity=50);
        background-color: #CCC;
        width: 100%;
        height: 100%;
        zoom: 1;
        background: #fbfbfb url("<?php echo plugin_web_path('orangehrmDashboardPlugin', 'images/loading.gif') ?>") no-repeat center;
    }
</style>
<div class="box">
    <div class="head">
        <h1>Add Meeting Room</h1>
    </div>
    <div class="inner" id="addMeetingRoom">
        <form id="frmAddEmp" method="post" action="" enctype="multipart/form-data" novalidate="novalidate">
            <fieldset>
                <ol>
                    <li><label for="employeeId">Meeting Room Name</label>
                        <input class="formInputText" type="text" name="room_name" id="room_name" required>
                    </li>
                </ol>
                <p>
                    <input type="submit" class="" name="btnSave" value="Save">
                </p>
            </fieldset>
        </form>
    </div>
    <div class="inner">
        <div id="tableWrapper">
            <table class="table hover" id="resultTable">
                <thead>
                    <tr>
                        <th rowspan="1" style="width:30%"><span class="headerCell">Meeting Room</span></th>
                        <th rowspan="1" style="width:15%"><span class="headerCell">Action</span></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $meetingRooms = getMeetingRooms($conn);
                    if (mysqli_num_rows($meetingRooms) > 0) {
                        foreach ($meetingRooms as $key => $value) {
                            ?>
                    <tr>
                        <td><?= $value['name']; ?></td>
                        <td>
                            <a href='<?php echo url_for('room/editMeetingRoom?id=' . $value['id']); ?>'>Edit</a>
                            ||
                            <a href='<?php echo url_for('room/deleteMeetingRoom?id=' . $value['id']); ?>'>Delete</a>
                        </td>
                    </tr>
                    <?php

                }
            } else {
                ?>
                    <tr>
                        <td colspan="2">No Records Found</td>
                    </tr>
                    <?php

                }
                ?>

                </tbody>
            </table>
        </div>

    </div>
</div> 