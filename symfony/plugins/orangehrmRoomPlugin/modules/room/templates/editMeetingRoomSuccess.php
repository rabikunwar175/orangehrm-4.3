<?php include('_editRoom.php'); ?>
<?php echo stylesheet_tag(plugin_web_path('orangehrmDashboardPlugin', 'css/orangehrmDashboardPlugin.css')); ?>
<style type="text/css">
    .loadmask {
        top: 0;
        left: 0;
        -moz-opacity: 0.5;
        opacity: .50;
        filter: alpha(opacity=50);
        background-color: #CCC;
        width: 100%;
        height: 100%;
        zoom: 1;
        background: #fbfbfb url("<?php echo plugin_web_path('orangehrmDashboardPlugin', 'images/loading.gif') ?>") no-repeat center;
    }
</style>
<div class="box">
    <div class="head">
        <h1>Edit Meeting Room</h1>
    </div>
    <div class="inner" id="addMeetingRoom">
        <form id="frmAddEmp" method="post" action="" enctype="multipart/form-data" novalidate="novalidate">
            <fieldset>
                <ol>
                    <li><label for="employeeId">Meeting Room Name</label>
                        <input class="formInputText" type="text" name="room_name" id="room_name" value="<?= meetingRoomDetail($conn, $meetingRoomId);?>" required>
                    </li>
                </ol>
                <p>
                    <input type="submit" class="" name="btnSave" value="Save">
                </p>
            </fieldset>
        </form>
    </div>
</div> 