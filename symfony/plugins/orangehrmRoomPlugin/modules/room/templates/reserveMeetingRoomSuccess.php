<?php include('_bookRoom.php'); ?>
<?php echo stylesheet_tag(plugin_web_path('orangehrmDashboardPlugin', 'css/orangehrmDashboardPlugin.css')); ?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    jQuery(function () {
        jQuery(".date-picker").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: new Date()
        });
        $('#from-time').timepicker({
            timeFormat: 'HH:mm',
            interval: 15,
            minTime: '08:00',
            maxTime: '17:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
        $('#to-time').timepicker({
            timeFormat: 'HH:mm',
            interval: 15,
            minTime: '08:00',
            maxTime: '17:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    });

</script>
<style type="text/css">
    .loadmask {
        top: 0;
        left: 0;
        -moz-opacity: 0.5;
        opacity: .50;
        filter: alpha(opacity=50);
        background-color: #CCC;
        width: 100%;
        height: 100%;
        zoom: 1;
        background: #fbfbfb url("<?php echo plugin_web_path('orangehrmDashboardPlugin', 'images/loading.gif') ?>") no-repeat center;
    }
</style>
<div class="box">
    <div class="head">
        <?php
        $isSuccess = false;
        $message   = "";
        if (isset($_SESSION["success_msg"])) {
            $isSuccess = true;
            $message   = $_SESSION["success_msg"];
            unset($_SESSION["success_msg"]);
        } elseif (isset($_SESSION["error_msg"])) {
            $isSuccess = false;
            $message   = $_SESSION["error_msg"];
            unset($_SESSION["error_msg"]);
        }
        if (!empty($message)) { ?>
            <div class="<?php echo($isSuccess ? 'success' : 'fail') ?>">
                <b><?= $message ?></b>
            </div>
            <?php
        } ?>
        <h1>Book Meeting Room</h1>
    </div>
    <div class="inner" id="addEmployeeTbl">
        <form id="frmAddBooking" method="post" action="" novalidate="novalidate">
            <fieldset>
                <ol>
                    <li><label for="meeting-room">Meeting Room</label>
                        <select required id="meeting-room" name="meeting_room_id">
                            <?php
                            $meetingRoomList = getMeetingRoomsList($conn);
                            if (mysqli_num_rows($meetingRoomList) > 0) {
                                foreach ($meetingRoomList as $key => $value) {
                                    ?>
                                    <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                    <?php
                                }
                            } ?>
                        </select>
                    </li>
                    <li><label for="date">Date</label>
                        <input required readonly id="date" class="date-picker formInputText" type="text" name="date">
                    </li>
                    <li><label for="from-time">From Time</label>
                        <input required readonly id="from-time" class="formInputText time-picker" type="text" name="from_time">
                    </li>
                    <li><label for="to-time">To Time</label>
                        <input required readonly id="to-time" class="formInputText time-picker" type="text" name="to_time">
                    </li>
                </ol>
                <p>
                    <input type="submit" class="" name="btnSave" value="Save">
                </p>
            </fieldset>
        </form>
    </div>
</div> 