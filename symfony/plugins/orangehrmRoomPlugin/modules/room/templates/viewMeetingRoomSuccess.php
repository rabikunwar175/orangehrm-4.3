<?php include('_process.php'); ?>
<?php echo stylesheet_tag(plugin_web_path('orangehrmDashboardPlugin', 'css/orangehrmDashboardPlugin.css')); ?>
<style type="text/css">
    .loadmask {
        top: 0;
        left: 0;
        -moz-opacity: 0.5;
        opacity: .50;
        filter: alpha(opacity=50);
        background-color: #CCC;
        width: 100%;
        height: 100%;
        zoom: 1;
        background: #fbfbfb url("<?php echo plugin_web_path('orangehrmDashboardPlugin', 'images/loading.gif') ?>") no-repeat center;
    }
</style>
<div class="box">
    <div class="head">
        <h1><?php echo __('Meeting Room Management'); ?></h1>
    </div>
    <div class="inner">
        <div id="tableWrapper">
            <table class="table hover" id="resultTable">
                <thead>
                    <tr>
                        <th rowspan="1" style="width:15%"><span class="headerCell">Meeting Room</span></th>
                        <th rowspan="1" style="width:15%"><span class="headerCell">Request By</span></th>
                        <th rowspan="1" style="width:5%"><span class="headerCell">Status</span></th>
                        <th rowspan="1" style="width:30%"><span class="headerCell">Time Range</span></th>                        
                        <th rowspan="1" style="width:15%"><span class="headerCell">Action</span></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (mysqli_num_rows($bookings) > 0) {
                        foreach ($bookings as $key => $value) {
                            ?>
                    <tr>
                        <td><?= $value['name']; ?></td>
                        <td><?= $value['booked_by']; ?></td>
                        <td><?= $value['status']; ?></td>
                        <td><?= $value['time_range']; ?></td>
                        <td>
                            <select name="booking_status" id="booking_status" data-bId="<?= $value['id']; ?>">
                                <option value="">-- Update --</option>
                                <option value="approved">Approved</option>
                                <option value="cancelled">Cancelled</option>
                            </select>
                        </td>
                    </tr>
                    <?php

                }
            } else {
                ?>
                    <tr>
                        <td colspan="6">No Records Found</td>
                    </tr>
                    <?php

                }
                ?>

                </tbody>
            </table>
        </div>

    </div>
</div>
<script>
    jQuery(document).ready(function() {
        $('select').on('change', function() {
            var r = confirm("Are you sure to take action!");
            if (r == true) {
                if (this.value != '') {

                    var data = {
                        bookingId: $(this).data('bid'),
                        action: this.value
                    };

                    var url = '<?= url_for('room/verifyBooking'); ?>';
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        method: 'post',
                        data: data,
                        success: function(response) {
                            location.reload();
                        },
                        error: function(response) {
                            alert('Failed to update');
                            location.reload();
                        }
                    });
                }
            }



        });

    });
</script> 